package ua.nure.beizerov.practice1;


/**
 * This class is represent a Part7.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part7 {

	private static final int NUM_SYSTEM_BASE_26 = 26;

	/* 64 is the number of characters before the 'A'
		 character in the Unicode table */
	private static final int SEED_64 = 64;
	
	
	private static final int NO_MAGIC_1 = 1;
	private static final int NO_MAGIC_2 = 2;
	private static final int NO_MAGIC_26 = 26;
	private static final int NO_MAGIC_27 = 27;
	private static final int NO_MAGIC_52 = 52;
	private static final int NO_MAGIC_53 = 53;
	private static final int NO_MAGIC_702 = 702;
	private static final int NO_MAGIC_703 = 703;


	public static void main(String[] args) {
		String arrow = " ==> ";
		
		System.out.println(
			"A ==> " + str2int("A") + arrow + int2str(NO_MAGIC_1)
		);
		System.out.println(
			"B ==> " + str2int("B") + arrow + int2str(NO_MAGIC_2)
		);
		System.out.println(
			"Z ==> " + str2int("Z") + arrow + int2str(NO_MAGIC_26)
		);
		System.out.println(
			"AA ==> " + str2int("AA") + arrow + int2str(NO_MAGIC_27)
		);
		System.out.println(
			"AZ ==> " + str2int("AZ") + arrow + int2str(NO_MAGIC_52)
		);
		System.out.println(
			"BA ==> " + str2int("BA") + arrow + int2str(NO_MAGIC_53)
		);
		System.out.println(
			"ZZ ==> " + str2int("ZZ") + arrow + int2str(NO_MAGIC_702)
		);
		System.out.println(
			"AAA ==> " + str2int("AAA") + arrow + int2str(NO_MAGIC_703)
		);
	}

	public static int str2int(String number) {
		int intRepresentation = 0;

		for (int i = 0, order = number.length() - 1, length = number.length()
			; i < length
			; i++, order--
		) {
			intRepresentation +=   
				(number.charAt(i) - SEED_64) 
					* Math.pow(NUM_SYSTEM_BASE_26, order);
		}

		return intRepresentation;
	}

	private static String stringReverse(String str) {
		char[] characters = new char[str.length()];

		for (int i = 0, fromTheEnd = str.length() - 1
			; fromTheEnd > -1
			; i++, fromTheEnd--
		) {
			characters[i] = str.charAt(fromTheEnd);
		}

		return new String(characters);
	}

	public static String int2str(int number) {
		StringBuilder strBuilderNumber = new StringBuilder();

		for (; number != 0; number /= NUM_SYSTEM_BASE_26) {
			if (number / NUM_SYSTEM_BASE_26 != 0) {

				if (number % NUM_SYSTEM_BASE_26 == 0) {
					strBuilderNumber.append(
						(char) (NUM_SYSTEM_BASE_26 + SEED_64)
					);

					number--;
				} else {
					strBuilderNumber.append(
						(char) (number % NUM_SYSTEM_BASE_26 + SEED_64)
					);					
				} 

			} else {
				strBuilderNumber.append(
					(char) (number % NUM_SYSTEM_BASE_26 + SEED_64)
				);
			} 
		}

		return stringReverse(strBuilderNumber.toString());
	}

	public static String rightColumn(String number) {	
		return int2str(str2int(number) + 1);
	}
}
