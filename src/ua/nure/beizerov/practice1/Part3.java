package ua.nure.beizerov.practice1;



/**
 * This class is represent a Part3.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part3 {

	public static void main(String[] args) {
		StringBuilder output = new StringBuilder();
		
		for (String str : args) {
			output.append(String.format("%s ", str));
		}
		
		System.out.println(output.toString().trim()); 
	}
}
