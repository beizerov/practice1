// encoding = UTF-8
package ua.nure.beizerov.practice1;



/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) {
		Part1.main(null);
		Part2.main(new String[] {"22", "15", "10", "11"});
		Part3.main(new String[] {"Java", "is", "a", "cool", "language!", "😉"});
		Part4.main(new String[] {"100", "25"});
		Part5.main(new String[] {"12341034"});
		Part6.main(new String[] {"10"});
		Part7.main(null);
	}
}
