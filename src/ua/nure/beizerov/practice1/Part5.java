package ua.nure.beizerov.practice1;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part5 {

	public static void main(String[] args) {
		int digitsSum = 0;

		for (int i = 0, numberCount = args[0].length()
			; i < numberCount
			; i++
		) {
			digitsSum += Integer.parseInt(
				String.valueOf(args[0].charAt(i))
			);
		}

		System.out.println(digitsSum);
	}
}
