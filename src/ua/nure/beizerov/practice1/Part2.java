package ua.nure.beizerov.practice1;


/**
 * This class is represent a Part2.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part2 {

	public static void main(String[] args) {
		int sum = 0;
		
		for (String strNumber : args) {
			sum += Integer.parseInt(strNumber);
		}
		
		System.out.println(sum);
	}
}
