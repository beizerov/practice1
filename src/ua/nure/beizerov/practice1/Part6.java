package ua.nure.beizerov.practice1;


/**
 * This class is represent a Part6.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part6 {

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int[] integerArray = new int[n];
	
		for (int i = 0, startPoint = 1
			; i < n
			; i++, startPoint = integerArray[i - 1]
		) {
			integerArray[i] = getPrimeNumber(startPoint);
		}
		
		StringBuilder output = new StringBuilder();
		
		for (int i : integerArray) {
			output.append(String.format("%d ", i));
		}
	
		System.out.println(output.toString().trim()); 
	}

	/**
	 * This method checks the number is a prime number.
	 *
	 * @param number is the value to check.
	 * @return True, if it is really a prime number.
	 */
	private static boolean isPrimeNumber(int number) {
		for (int i = 2; i < number; i++) {
			if (number %  i == 0) {
				return false;
			}
		}

		return true;
	}

	/**
	 * This method serves to get a prime number.
	 * 
	 * @param startPoint is the numeric point
	 *		  from which the next prime number will be searched.
	 * @return A prime number.
	 */
	private static int getPrimeNumber(int startPoint) {
		for (int i = startPoint + 1; ; i++) {
			if (isPrimeNumber(i)) {
				return i;		
			}
		}
	}
}
