package ua.nure.beizerov.practice1;


import static java.lang.Math.min;


/**
 * This class is represent a Part4.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part4 {

	public static void main(String[] args) {
		int firstIntNumber = Integer.parseInt(args[0]);
		int secondIntNumber = Integer.parseInt(args[1]);
	
		// greatest common factor
		int gcd = 1;
	
		for (int i = 2, smaller = min(firstIntNumber, secondIntNumber)
			; i <= smaller
			; i++
		) {
			if (firstIntNumber % i == 0 && secondIntNumber % i == 0) {
				gcd = i;
			}
		}
	
		System.out.println(gcd);
	}
}
